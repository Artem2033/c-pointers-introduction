#include <stdio.h>
#include <stdlib.h>



int global_int1 = 1; // Under the heap
int global_int2 = 2; 
int global_int3 = 3; 

void main (void)
{
	int local_int1 = 1; // Stack
	int local_int2 = 2;
	int local_int3 = 3;
	
	printf("Global int 1 addr=%p val=%d\n", &global_int1, global_int1);
	printf("Global int 2 addr=%p val=%d\n", &global_int2, global_int2);
	printf("Global int 3 addr=%p val=%d\n", &global_int3, global_int3);
	
	printf("----------------------------------------------------\n");
	
	printf("Local int 1 addr=%p val=%d\n", &local_int1, local_int1);
	printf("Local int 2 addr=%p val=%d\n", &local_int2, local_int2);
	printf("Local int 3 addr=%p val=%d\n", &local_int3, local_int3);
	
	printf("----------------------------------------------------\n");
	
	int* local_intp1;
	int* local_intp2;
	int* local_intp3;
	
	local_intp1 = malloc(sizeof(int)); // HEAP
	local_intp2 = malloc(sizeof(int));
	local_intp3 = malloc(sizeof(int));
	
	*local_intp1 = 1;
	*local_intp2 = 2;	
	*local_intp3 = 3;
	

	
	printf("Int on heap 1 addr=%p val=%d\n", local_intp1, *local_intp1);
	printf("Int on heap 2 addr=%p val=%d\n", local_intp2, *local_intp2);
	printf("Int on heap 3 addr=%p val=%d\n", local_intp3, *local_intp3);
	
	printf("----------------------------------------------------\n");
	
	printf("Pointer to int on heap 1 addr=%p val=%p\n", &local_intp1, local_intp1);
	printf("Pointer to int on heap 2 addr=%p val=%p\n", &local_intp2, local_intp2);
	printf("Pointer to int on heap 3 addr=%p val=%p\n", &local_intp3, local_intp3);
	
	
	free(local_intp1);
	free(local_intp2);
	free(local_intp3);

}


