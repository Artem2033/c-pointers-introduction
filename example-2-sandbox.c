#include <stdio.h>
#include <stdlib.h>


void main (void)
{
	int i;
	char* storage;
	storage = malloc(sizeof(char) * 32);
	
	
	int* int_p;
	int_p = (int*) storage;
	*int_p = 0xFF112233;
	
	
	char* storage2 = storage+2;
	
	int_p = (int*) storage2;
	*int_p = 0xFF112233;

	
	
	printf("Pos : Address    : Value\n");
	
	for (i=0; i< 32; i++)
	{
		printf("0x%08X : 0x%02X\n", (storage+i), (unsigned char)(*(storage+i)));
	}
	
	free(storage);
}


