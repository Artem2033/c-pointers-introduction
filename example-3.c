#include <stdio.h>
#include <stdlib.h>



void add(int a, int b, int* result)
{
	*result = a+b;
}

void main (void)
{
	int first = 3;
	int second = 5;

	int result;
	
	add( first, second, &result);
	
	printf ( "Result: %d", result);
}

