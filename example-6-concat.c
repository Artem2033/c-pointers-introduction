#include <stdio.h>
#include <stdlib.h>



void concat(	char* 	a, 
				int 	a_len, 
				char* 	b, 
				int 	b_len,
				char**	result, 
				int* 	result_len
		)
{
	int i;
	int r = 0;
	*result_len = a_len+b_len+1;
	*result = malloc(sizeof(char)*(*result_len));
	
	for (i=0;i<a_len;i++){
		*(*result+r) = *(a+i);
		r++;
	}
	
	for (i=0;i<b_len;i++){
		*(*result+r) = *(b+i);
		r++;
	}
	*(*result+r) = '\0';
}

void main (void)
{
	char* 	first;
	int 	first_len;
	
	char* 	second;
	int 	second_len;
	
	char*	result;
	int 	result_len;
	
	first = malloc(sizeof(char)*40);
	second = malloc(sizeof(char)*40);

	
	*first 		= 'H';
	*(first+1) 	= 'e';
	*(first+2) 	= 'l';
	*(first+3) 	= 'l';
	*(first+4) 	= 'o';
	*(first+5) 	= ' ';
	*(first+6) 	= '\0';
	
	first_len = 6;
	
	*second 	= 'W';
	*(second+1) = 'o';
	*(second+2) = 'r';
	*(second+3) = 'l';
	*(second+4) = 'd';
	*(second+5) = '\0';
	
	second_len = 5;
	
	concat(first, first_len, second, second_len, &result, &result_len);

	printf ( "Result: %s \n", result);
	
	free(first);
	free(second);
	free(result);
}

